                           [Basic Queries]

Problem 1 : Get all the names of students in the database.

            select name from students;

            "Alex"
            "Basma"
            "Hasan"
            "Jana"
            "Layal"
            "Robert"

____________________________________________________________________________
Problem 2 : Get all the data of students above 30 years old.

            select * from students where age > 30;

            "5"	"Robert"	"34"	"M"	"500"
            "6"	 "Jana"	        "33"	"F"	"500"

_____________________________________________________________________________
problem 3 : Get the names of the females who are 30 years old.

            select name from students where Gender = "F" and Age > 30;

            Jana

_____________________________________________________________________________
problem 4 : Get the number of Points of Alex.
            
            select points from students where name="Alex";

            200
______________________________________________________________________________
problem 5 : Add yourself as a new student (your name, your age...).

           insert into students values (7,'Hussein',25,'M',150);

           "7"	"Hussein"	"25"	"M"	"150"

______________________________________________________________________________
problem 6 : Increase the points of Basma because she solved a new exercise.

            update students set points = points + 100 where name = "Basma";

            "Basma"  "400"   (100 points added to original 300).

_______________________________________________________________________________
problem 7 : Decrease the points of Alex because he's late today.

            update students set points = points - 50 where name = "Alex";

            "Alex"  "150"   (50 points removed from original 200).

______________________________________________________________________________
------------------------------------------------------------------------------
______________________________________________________________________________
                             [Creating Tables] 

Create table graduates.
                CREATE TABLE `graduates` (
	`ID`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	`Name`	TEXT NOT NULL UNIQUE,
	`Age`	INTEGER,
	`Gender`	TEXT,
        `Points` INTEGER,
	`Graduation`	TEXT
);

_______________________________________________________________________________
Problem 1(8) : Copy Layal's data from students to graduates.

           insert into graduates (ID,Name,Age,Gender,Points)  
           select * from students where name = "Layal";

           "4"	"Layal"	"18"	"F"	"350"

________________________________________________________________________________
Problem 2(9) :Add the graduation date previously mentioned to Layal's record in graduates.

           update graduates set Graduation = "08/09/2018" where name = "Layal";
  
 
           "4"   "Layal"   "18"  "F"	"350"	"08/09/2018"
_____________________________________________________________________________________
problem 3(10) : Remove Layal's record from students.

            delete from students where name = "Layal";

      	"1"	"Alex"	    "21"	"M"	"150"
	"2"	"Basma"	    "25"	"F"	"400"
	"3"	"Hasan"	    "30"	"M"	"150"
         -------------DELETED RECORD "LAYAL"---------
	"5"	"Robert"    "34"        "M"	"500"
	"6"	"Jana"	    "33"	"F"	"500"
	"7"	"Hussein"   "25"	"M"	"150"

_________________________________________________________________________________________
----------------------------------------------------------------------------------------- 
_________________________________________________________________________________________
                                    [Joins]

problem 11 : Produce a table that contains, for each employee,
             his/her name, company name, and company date.

             select employees.Name,companies.Name,companies.Date from companies,employees  
             where companies.Name = employees.Company;


             "Marc"	"Google"	"1998"
             "Maria"	"Google"	"1998"
             "Alaa"	"Facebook"	"2004"
             "Hala"	"Snapchat"	"2011"

____________________________________________________________________________________________
problem 12 : Find the name of employees that work in companies made before 2000.

             select employees.Name from companies,employees  
             where companies.Name = employees.Company and companies.Date < 2000;

             "Marc"
             "Maria"

____________________________________________________________________________________________
problem 13 : Find the name of company that has a graphic designer.

             select companies.Name from companies,employees 
             where companies.Name = employees.Company and Role = "Graphic Designer";

             Snapchat

____________________________________________________________________________________________
--------------------------------------------------------------------------------------------
____________________________________________________________________________________________
                                         [Count & Filter]

problem 14 : Find the person with the highest number of points in students.

             select name,max(points) from students;

             "Robert"	"500"
____________________________________________________________________________________________
problem 15 : Find the average of points in students.

             select avg(points) from students;

             308.333333333333

____________________________________________________________________________________________
problem 16 : Find the number of students that have 500 points.

             select count(*) from students where points = 500;

             2

_____________________________________________________________________________________________
problem 17 : Find the names of students that contains 's.

             select name from students where name like '%s%';
 
            "Basma"
            "Hasan"
            "Hussein" 

_____________________________________________________________________________________________	           
problem 18 : Find all students based on the decreasing order of their points.

             select * from students order by points desc;

             "5"	"Robert"	"34"	"M"	"500"
             "6"	"Jana"	        "33"	"F"	"500"
             "2"	"Basma"	        "25"	"F"	"400"
             "1"	"Alex"	        "21"	"M"	"150"
             "3"	"Hasan"	        "30"	"M"	"150"
             "7"	"Hussein"	"25"	"M"	"150"
             
_________________________________________________________________________________________________
-------------------------------------------------------------------------------------------------
__________________________________________"THANK YOU"____________________________________________


             

  
